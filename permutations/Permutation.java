package permutations;

class Permutation {

    // Attributes
    private int length; // The number of elements in the permutation
    private int[] map;  // The mapping between elements

    /**
     * Constructs a permutation of having 'length' elements
     * and given mapping.
     *
     * @param length  The number of elements
     * @param map     An array representing the mapping, which
     *                must have the given length
     */
    Permutation(int length, int[] map) {
        this.length = length;
        this.map = map;
    }

    /**
     * Creates the identity permutation over
     * {0, 1, ..., n - 1}.
     *
     * @param n  The number of elements in the
     *           permutation
     */
    Permutation(int n) {
        this.length = n;
        this.map = new int[n];
        for (int i = 0; i < n; ++i) {
            this.map[i] = i;
        }
    }

    /**
     * Clone of this permutation.
     *
     * @return  A copy of the permutation
     */
    public Permutation clone() {
        int[] map = new int[length];
        for (int i = 0; i < length; ++i) {
            map[i] = this.map[i];
        }
        return new Permutation(length, map);
    }

    /**
     * Returns a string representation of this
     * permutation.
     *
     * @return  A string representing the permutation
     */
    public String toString() {
        String s = "[ ";
        for (int i = 0; i < this.length; ++i) {
            s += String.valueOf(get(i)) + " ";
        }
        s += "]";
        return s;
    }

    /**
     * Swaps the elements at positions i and j.
     *
     * @param i  The first position
     * @param j  The second position
     */
    public void swap(int i, int j) {
        int temp;
        temp = this.map[i];
        this.map[i] = this.map[j];
        this.map[j] = temp;
    }

    /**
     * Returns the element at position i.
     *
     * @param i  The position of the element
     * @return   The element
     */
    int get(int i) {
        return this.map[i];
    }

}
