package permutations;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class PermutationsIterator implements Iterator<Permutation> {

    // Attributes
    int n;                   // The number of elements
    Permutation permutation; // The current permutation
    int[] direction;         // The directions of the permutation
    int[] position;          // The inverse permutation
    boolean done;            // If true, then all permutations have been enumerated

    /**
     * Constructs an iterator over all permutations on the
     * set {0, 2, ..., n-1}.
     *
     * @param n  The number of elements in the
     *           permutation
     */
    public PermutationsIterator(int n) {
        this.n = n;
        this.direction = new int[n];
        this.position = new int[n];
        for (int i = 0; i < n; ++i) {
            this.position[i] = i;
            this.direction[i] = -1;
        }
        this.direction[0] = 0;
        this.permutation = new Permutation(n);
        this.done = false;
    }

    /**
     * Returns true if and only if there is a permutation
     * that has not been enumerated.
     *
     * @return  A boolean indicating if there is a permutation
     *          remaining
     */
    public boolean hasNext() {
        return !done;
    }

    /**
     * Remove method should not be used.
     */
    public void remove() {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns the next permutation.
     *
     * @return  A permutation that has not been returned before
     */
    public Permutation next() {
        int i, j;
        Permutation p = this.permutation.clone();
        
        if (done) {
            throw new NoSuchElementException();
        } else {
            i = this.n - 1;
            // Spots the greatest directed element
            while (i != 0 && direction[i] == 0) {
                --i;
            }
            // Checks if all elements are undirected
            if (i == 0) {
                done = true;
            } else {
                // Get adjacent element
                j = permutation.get(position[i] + direction[i]);
                // Update direction of jth-element
                if (position[j] == 0 || position[j] == n - 1 ||
                    permutation.get(position[j] + direction[i]) > i) {
                    direction[i] = 0;
                }
                this.swap(i, j);
                // Update direction of large elements
                j = i + 1;
                while (j < n) {
                    if (position[i] < position[j]) {
                        direction[j] = -1;
                    } else {
                        direction[j] = +1;
                    }
                    ++j;
                }
            }
            return p;
        }
    }

    /**
     * Swaps the elements i and j in the current permutation.
     *
     * @param i  The first element to swap
     * @param j  The second element to swap
     */
    private void swap(int i, int j) {
        int temp;

        this.permutation.swap(position[i], position[j]);
        temp = position[i];
        position[i] = position[j];
        position[j] = temp;
    }

    /**
     * Returns a string representation of this permutations
     * iterator.
     *
     * Used mostly for debugging.
     *
     * @return  The string representation
     */
    public String toString() {
        String s = "Permutations iterator\n";
        s += " Current permutation: " + this.permutation + "\n";
        s += " Inverse permutation: [ ";
        for (int i = 0; i < n; ++i) {
            s += position[i] + " ";
        }
        s += "]\n";
        s += " Directions:          [ ";
        for (int i = 0; i < n; ++i) {
            s += direction[i] + " ";
        }
        s += "]\n";
        return s;
    }

    /**
     * Main.
     *
     * @param args  Only one argument accepted (not validated)
     *              which is the order of the permutation.
     */
    public static void main(String[] args) {
        int n;
        PermutationsIterator permutations;

        n = new Integer(args[0]);
        permutations = new PermutationsIterator(n);
        while (permutations.hasNext()) {
            System.out.println(permutations.next());
        }
    }
}
