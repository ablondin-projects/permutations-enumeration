~~~~~~~~~~~~
Permutations
~~~~~~~~~~~~

A Java package to enumerate permutations based on Steinhaus, Johnson and
Trotter's algorithm, with Even's improvement.

See the `Wikipedia entry
<http://en.wikipedia.org/wiki/Steinhaus%E2%80%93Johnson%E2%80%93Trotter_algorithm>`__
for more details.

=====
Usage
=====

Enumerating all permutations on 4 elements::

     $ javac permutations/*.java && java permutations/PermutationsIterator 4
     [ 0 1 2 3 ]
     [ 0 1 3 2 ]
     [ 0 3 1 2 ]
     [ 3 0 1 2 ]
     [ 3 0 2 1 ]
     [ 0 3 2 1 ]
     [ 0 2 3 1 ]
     [ 0 2 1 3 ]
     [ 2 0 1 3 ]
     [ 2 0 3 1 ]
     [ 2 3 0 1 ]
     [ 3 2 0 1 ]
     [ 3 2 1 0 ]
     [ 2 3 1 0 ]
     [ 2 1 3 0 ]
     [ 2 1 0 3 ]
     [ 1 2 0 3 ]
     [ 1 2 3 0 ]
     [ 1 3 2 0 ]
     [ 3 1 2 0 ]
     [ 3 1 0 2 ]
     [ 1 3 0 2 ]
     [ 1 0 3 2 ]
     [ 1 0 2 3 ]

